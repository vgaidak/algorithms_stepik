from bisect import bisect, bisect_left


def qsort(inlist):
    less = []
    equal = []
    greater = []
    if not inlist:
        return []
    else:
        pivot_idx = len(inlist) // 2
        pivot = inlist[pivot_idx]
        for x in inlist:
            if x < pivot:
                less.append(x)
            elif x == pivot:
                equal.append(x)
            elif x > pivot:
                greater.append(x)
        return qsort(less) + equal + qsort(greater)


def how_many_segments_contains_point(point, left_ends, right_ends):
    lesser = bisect(left_ends, point)
    greater = bisect_left(right_ends, point)
    return lesser - greater


def main():
    result = []
    left_ends = []
    right_ends = []
    segment_number, point_number = map(int, input().split())
    for n in range(segment_number):
        a, b = map(int, input().split())
        left_ends.append(a)
        right_ends.append(b)
    left_ends_sorted = qsort(left_ends)
    right_ends_sorted = qsort(right_ends)
    points = map(int, input().split())
    for point in points:
        result.append(how_many_segments_contains_point(point, left_ends_sorted, right_ends_sorted))
    print(*result)


if __name__ == "__main__":
    main()

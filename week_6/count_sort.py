def count_sort(numbers, count):
    cnt = (max(numbers) + 1) * [0]
    for i in range(count):
        cnt[numbers[i]] += 1
    pos = 0
    for num in range(len(cnt)):
        for i in range(cnt[num]):
            numbers[pos] = num
            pos += 1
    return numbers


def main():
    count = int(input())
    numbers = [int(x) for x in input().split()]
    result = count_sort(numbers, count)
    print(*result)


if __name__ == "__main__":
    main()

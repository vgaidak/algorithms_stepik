def find_index(n, first_line, k):
    l = 0
    r = n - 1
    while l <= r:
        m = int((l + r) / 2)
        if first_line[m] == k:
            return m + 1
        elif first_line[m] > k:
            r = m - 1
        else:
            l = m + 1
    return -1


def main():
    n, *first_line = map(int, input().split())
    k, *second_line = map(int, input().split())

    indexes = []
    for k in second_line:
        indexes.append(find_index(n, first_line, k))

    print(*indexes)


if __name__ == "__main__":
    main()

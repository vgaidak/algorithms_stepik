import pytest


def test():
    assert lis([3, 6, 12, 7, 9, 24, 18, 3, 9, 242], 10) == 5


def lis(a, n):
    d = [i for i in range(1, n + 1)]

    for i in range(1, n):
        d[i] = 1
        for j in range(0, i):
            if (a[j] <= a[i]) and (d[i] < d[j] + 1) and (a[i] % (a[j]) == 0):
                d[i] = d[j] + 1
    ans = 0
    for i in range(1, n):
        ans = max(ans, d[i])
    return ans


def main():
    n = int(input())
    a_list = list(map(int, input().split()))
    print(lis(a_list, n))


if __name__ == "__main__":
    main()

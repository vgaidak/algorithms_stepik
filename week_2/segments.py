def segments(seg_list):
    points = []
    seg_list = sorted(seg_list, key=lambda x: x[1])
    i = 0
    while i < len(seg_list) - 1:
        j = 1
        while seg_list[i][1] > seg_list[i + j][0]:
            if i + j < len(seg_list) - 1:
                j += 1
            else:
                break
        points.append(seg_list[i][1])
        i = i + j
    return len(points), points


def main():
    seg_list = []
    n = int(input())
    i = 1
    while i <= n:
        i += 1
        k, m = map(int, input().split())
        seg_list.append((k, m))
    count, points = segments(seg_list)
    print(count)
    print(*points)


if __name__ == "__main__":
    main()



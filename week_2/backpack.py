def backpack(things, volume):
    things = sorted(things, key=lambda x: x[0]/x[1], reverse=True)
    v = 0
    cost = 0
    for t in things:
        if v < volume:
            if t[1] <= volume - v:
                v += t[1]
                cost += t[0]
            else:
                cost += t[0]/t[1]*(volume - v)
                v = volume
        else:
            break
    return round(cost, 3)


def main():
    things = []
    n, volume = map(int, input().split())
    i = 1
    while i <= n:
        i += 1
        c, v = map(int, input().split())
        things.append((c, v))
    cost = backpack(things, volume)
    print(cost)


if __name__ == "__main__":
    main()


# 5 9022
# 3316 1601
# 5375 8940
# 2852 6912
# 3336 9926
# 1717 8427
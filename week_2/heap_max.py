from heapq import _heapify_max as heapify_max
from heapq import _siftdown_max, _siftup_max

x = []
heapify_max(x)


def heappush_max(heap, item):
    """Push item onto heap, maintaining the heap invariant."""
    heap.append(item)
    _siftdown_max(heap, 0, len(heap) - 1)


def heappop_max(heap):
    """Maxheap version of a heappop."""
    lastelt = heap.pop()    # raises appropriate IndexError if heap is empty
    if heap:
        returnitem = heap[0]
        heap[0] = lastelt
        _siftup_max(heap, 0)
        return returnitem
    return lastelt


def extract_max():
    print(heappop_max(x))


def insert_number(number):
    heappush_max(x, number)


def main():
    ops = []
    op_count = int(input())
    for i in range(op_count):
        ops.append(str(input()))
    for op in ops:
        if op == 'ExtractMax':
            extract_max()
        else:
            insert_number(int(op.split()[1]))


if __name__ == "__main__":
    main()

def decode_string(code, str_to_decode):
    code_keys = list(code.keys())
    decoded_str = ''
    symbols = list(str_to_decode)
    i = 0
    sym = ''
    while i < len(symbols):
        sym += symbols[i]
        if sym in code_keys:
            decoded_str += code[sym]
            sym = ''
        i += 1
    print(decoded_str)


def main():
    code = {}
    while True:
        letter = input()
        if ":" in letter:
            code[letter.split()[1]] = letter.split()[0][:-1]
        else:
            if len(letter.split()) == 1:
                str_to_decode = letter
                break
    decode_string(code, str_to_decode)


if __name__ == "__main__":
    main()

fibs_last = [0, 1]


def fib_digit(n):
    i = 0
    while i <= n:
        fibs_last.append((fibs_last[i] + fibs_last[i + 1]) % 10)
        i += 1
    return fibs_last[n]


def main():
    n = int(input())
    print(fib_digit(n))


if __name__ == "__main__":
    main()

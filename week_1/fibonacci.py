fib_numbers = [0, 1]

def fib(n):
    i = 1
    while i <= n:
        fib_numbers.append(fib_numbers[i] + fib_numbers[i-1])
        i += 1
    return fib_numbers[n - 1]


def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()
fibs = {0: 0, 1: 1}
pizzano_mods = [0, 1]


def check_pi(mod_list):
    if len(mod_list) > 2:
        if mod_list[-1] == 1 and mod_list[-2] == 0:
            return True
    return False


def fib_mod(n, m):
    i = 0
    while check_pi(pizzano_mods) is False:
        fibs[0], fibs[1] = fibs[1], fibs[0] + fibs[1]
        pizzano_mods.append(fibs[1] % m)
        i += 1
    pizzano_period = len(pizzano_mods) - 2
    return pizzano_mods[n % pizzano_period - 1]


def main():
    n, m = map(int, input().split())
    print(fib_mod(n, m))


if __name__ == "__main__":
    main()
